# Introduction

TONE is a write tail latency optimized learned index. 
It is built as an extension to ALEX, 
where the original work of ALEX could be found from 
[here](https://github.com/microsoft/ALEX).

# Main Contributions
For our workshop submission, we have made the following contributions:
1. Two-level design Gapped Array
2. Node merge algorithm
3. Separation of recent and old key-value pairs

# Build
The build process of TONE is same to ALEX. Below is an overview for TONE.
```
# Build the whole project using CMake
./build.sh OR ./build_new.sh
```
We suggest use `./build.sh` at first. If you happen to run into any Windows line-endings issues on Mac OS. Please use `build_new.sh` for Mac OS instead of `build.sh`.

# Run
You could start TONE with benchmark file with the following command:
```
./build/benchmark \
--benchmarks=[benchmark type] \
--keys_file=[download location] \
--keys_file_type=binary \
--init_num_keys=100000 \
--total_num_keys=200000 \
--batch_size=10000 \
--insert_frac=0.5 \
--print_batch_stats \
```
You are free to change the benchmark type from this template. Note that some parameters are forced to pass
in even you might not use it, such as `keys_file`. For those parameters, you could pass a dummy parameter.
There are also parameter such as `insert_frac` which only impact the `default` benchmark.

A 200M-key (1.6GB) dataset can be downloaded from [Google Drive](https://drive.google.com/file/d/1zc90sD6Pze8UM_XYDmNjzPLqmKly8jKl/view?usp=sharing),
 which is used in ALEX. To run one example workload on this dataset with `--benchmarks=default`.
Note that you need set the `KEY_TYPE` as `double` for default benchmark. Test suits inherited from ALEX 
are not used at all.

Cleanup is still going on, so there could be somme issues resulting from code refactoring.