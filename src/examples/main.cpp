// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.

/*
 * This short sample program demonstrates ALEX's API.
 */

#include "../core/alex.h"

#define KEY_TYPE int
#define PAYLOAD_TYPE int

int main(int, char **) {
    // Create some synthetic data: keys are dense integers between 0 and 99, and
    // payloads are random values
    const int num_keys = 100;
    std::pair<KEY_TYPE, PAYLOAD_TYPE> values[num_keys];
    std::mt19937_64 gen(std::random_device{}());
    std::uniform_int_distribution<PAYLOAD_TYPE> dis;
    for (int i = 0; i < num_keys; i++) {
        values[i].first = i;
        values[i].second = i;
    }

    alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;

    // Bulk load the keys [0, 100)
    index.bulk_load(values, num_keys);

    std::cout << "Finished bulk loading!" << std::endl;

    for (int i = 110; i < 160; i++) {
        KEY_TYPE new_key = i;
        PAYLOAD_TYPE new_payload = i;
        index.insert(new_key, new_payload);
    }

    if (index.validate_structure(true)){
        std::cout << "Correct structure in the end!" << std::endl;
    }
}