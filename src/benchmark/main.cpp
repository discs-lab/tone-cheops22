// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.

/*
 * Simple benchmark that runs a mixture of point lookups and inserts on ALEX.
 */

#include "../core/alex.h"

#include <iomanip>

#include "flags.h"
#include "utils.h"
#include <ctime>
#include <unistd.h>
#include <fstream>
#include <filesystem>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <pthread.h>

// Modify these if running your own workload
#define KEY_TYPE int
#define PAYLOAD_TYPE double

/*
 * Required flags:
 * --keys_file              path to the file that contains keys
 * --keys_file_type         file type of keys_file (options: binary or text)
 * --init_num_keys          number of keys to bulk load with
 * --total_num_keys         total number of keys in the keys file
 * --batch_size             number of operations (lookup or insert) per batch
 * --benchmarks             type of benchmarks
 *
 * Optional flags:
 * --insert_frac            fraction of operations that are inserts (instead of lookups)
 * --lookup_distribution    lookup keys distribution (options: uniform or zipf)
 * --time_limit             time limit, in minutes
 * --print_batch_stats      whether to output stats for each batch
 * --hot_time_frame         how long will the hottness of keys last
 * --print_logs             whether print the stats every second
 */

static const char characters[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

int default_benchmarks(std::string keys_file_type, std::string keys_file_path,
                       int init_num_keys, int total_num_keys, int batch_size, double insert_frac,
                       std::string lookup_distribution, bool print_batch_stats, double time_limit);

// please note that the workloads given in this benchmark files are
// simply examples. They serve as a brief overview about how the workload run and
// do not reflect the workloads used in the paper
// you could build your own workloads based on them

int YCSBWorkloadA(std::string keys_file_type, std::string keys_file_path,
                  int init_num_keys, int batch_size, bool print_batch_stats, double time_limit, int value_size);

int ChangingWorkloadYCSB(int total_num_keys, int batch_size, double time_limit, int value_size, double hot_time_frame,
                         bool print_logs);

int ScanHeavy(int init_num_keys, int batch_size);

int YCSBWorkloadA_mt(int init_num_keys, int batch_size, int value_size);

int ChangingWorkloadYCSB_mt(int total_num_keys, int batch_size, double time_limit, int value_size, double hot_time_frame,
                            bool print_logs);

// this is a helper class for generating a random payload quickly
class RandomGenerator {
private:
    std::string data_;
    int total_size = 100000;

public:
    RandomGenerator() {
        srand((unsigned) time(NULL) * getpid());
        data_.reserve(total_size);
        for (int i = 0; i < total_size; ++i)
            data_ += characters[rand() % (sizeof(characters) - 1)];
    }

    std::string Generate(int value_size) {
        return data_.substr(0, value_size);
    }
};

// this is a simple barrier class to ensure all the
// threads start to work at the same time!
// https://stackoverflow.com/questions/38999911/what-is-the-best-way-to-realize-a-synchronization-barrier-between-threads
class Barrier {
public:
    Barrier(int count) {
        counter = 0;
        thread_count = count;
        waiting = 0;
    }

    void wait() {
        //fence mechanism
        std::unique_lock<std::mutex> lk(m);
        ++counter;
        ++waiting;
        cv.wait(lk, [&] { return counter >= thread_count; });
        cv.notify_one();
        --waiting;
        if (waiting == 0) {
            //reset barrier
            counter = 0;
        }
        lk.unlock();
    }

private:
    std::mutex m;
    std::condition_variable cv;
    int counter;
    int waiting;
    int thread_count;
};

class LatencyDistributionRecorder {
private:
    int bucket_size = 100;
    std::vector<long int> buckets;
    std::vector<int> high_latencies;
    double interval_size = 0;
    double nintynine = 0;
    double nintynine_nine = 0;
    double median = 0;

    // this is the expected response time in microseconds
    double expected_lower_bound = 0;
    double expected_upper_bound = 500;

public:
    LatencyDistributionRecorder(int bucket_size,
                                double expected_lower_bound, double expected_upper_bound) {
        this->bucket_size = bucket_size;
        this->expected_lower_bound = expected_lower_bound;
        this->expected_upper_bound = expected_upper_bound;
        this->interval_size = (expected_upper_bound - expected_lower_bound) / bucket_size;

        for (int i = 0; i < this->bucket_size + 2; i++) {
            buckets.push_back(0);
        }
    }

    bool add(double response_time) {
        if (response_time < interval_size) {
            buckets[0]++;
            return true;
        }
        for (int i = 1; i <= this->bucket_size; i++) {
            if (response_time > interval_size * i && response_time < interval_size * (i + 1)) {
                buckets[i]++;
                return true;
            }
        }
        if (response_time > interval_size * (bucket_size + 1)) {
            buckets.back()++;
            high_latencies.push_back(static_cast<int>(response_time));
            return true;
        }

        return false;
    }

    // this method is going to print the statistics
    void print_stats(bool verbose) {
        // this is used to calculate some important percentile stats
        long int num_total_inserts = 0;
        for (int i = 0; i < this->bucket_size + 2; i++) {
            num_total_inserts += buckets[i];
        }

        long int nintynine_threshold = num_total_inserts * 0.99;
        double nintynine_latency = 0;
        long int nintynine_nine_threshold = num_total_inserts * 0.999;
        double nintynine_nine_latency = 0;

        long int temp_counter = 0;
        for (int i = 0; i < this->bucket_size + 2; i++) {
            temp_counter += buckets[i];
            if (temp_counter > nintynine_threshold) {
                nintynine_latency = (interval_size * i + interval_size * (i + 1)) / 2;
                break;
            }
        }

        temp_counter = 0;
        for (int i = 0; i < this->bucket_size + 2; i++) {
            temp_counter += buckets[i];
            if (temp_counter > nintynine_nine_threshold) {
                nintynine_nine_latency = (interval_size * i + interval_size * (i + 1)) / 2;
                break;
            }
        }

        temp_counter = 0;
        double median_latency = 0;
        for (int i = 0; i < this->bucket_size + 2; i++) {
            temp_counter += buckets[i];
            if (temp_counter > num_total_inserts / 2) {
                median_latency = (interval_size * i + interval_size * (i + 1)) / 2;
                break;
            }
        }

        nintynine = nintynine_latency;
        nintynine_nine = nintynine_nine_latency;
        median = median_latency;
        std::cout << "\tThe 99p " << nintynine_latency << " microsec," << "\t99.9p " << nintynine_nine_latency
                  << " microsec," << "\tmedian " << median_latency << " microsec."
                  << std::endl;

        if (verbose) {
            // this is usually for plots
            // the printout would be similar to a csv file format for some stats
            std::cout << expected_lower_bound << ", " << expected_lower_bound + interval_size
                      << ", " <<
                      buckets[0] << std::endl;
            for (int i = 1; i <= this->bucket_size; i++) {
                std::cout << interval_size * i << ", " << interval_size * (i + 1) << ", " <<
                          buckets[i] << std::endl;
            }
            std::cout << expected_upper_bound << ", infinity, " <<
                      buckets.back() << std::endl;

            double num_remaining_factor = 0.9;
            std::cout << "-----------------------------------------------" << std::endl;
            std::cout << "Below are the response latenceis that exceed the expected upper bound." << std::endl;
            std::cout << "Only the first " << num_remaining_factor * 100 << " percent data are shown." << std::endl;

            std::sort(high_latencies.begin(), high_latencies.end());

            int num_remaining = static_cast<int>(this->high_latencies.size() * num_remaining_factor);
            for (int i = 0; i < num_remaining; i++) {
                std::cout << high_latencies[i] << ", ";
            }

            std::cout << high_latencies.back() << std::endl;
        }
    }

    void reset() {
        for (unsigned long i = 0; i < buckets.size(); i++) {
            buckets[i] = 0;
        }
    }

    double get_99p() {
        return nintynine;
    }

    double get_99_point_9p() {
        return nintynine_nine;
    }

    double get_median() {
        return median;
    }


    void print_latency_CDF() {
        for (int i = 0; i < this->bucket_size; i++) {
            std::cout << (interval_size * i + interval_size * (i + 1)) / 2 << ",  " << buckets[i] << std::endl;
        }
    }
};


// start to add up some YCSB workloads
int main(int argc, char *argv[]) {
    auto flags = parse_flags(argc, argv);
    std::string keys_file_path = get_required(flags, "keys_file");
    std::string keys_file_type = get_required(flags, "keys_file_type");
    std::string benchmarks = get_required(flags, "benchmarks");
    auto init_num_keys = stoi(get_required(flags, "init_num_keys"));
    auto total_num_keys = stoi(get_required(flags, "total_num_keys"));
    auto batch_size = stoi(get_required(flags, "batch_size"));
    auto insert_frac = stod(get_with_default(flags, "insert_frac", "0.5"));
    std::string lookup_distribution =
            get_with_default(flags, "lookup_distribution", "zipf");
    auto time_limit = stod(get_with_default(flags, "time_limit", "3"));
    bool print_batch_stats = get_boolean_flag(flags, "print_batch_stats");
    int value_size = stoi(get_with_default(flags, "value_size", "1000"));
    double hot_time_frame = stod(get_with_default(flags, "hot_time_frame", "5"));
    bool print_logs = get_boolean_flag(flags, "print_logs");

    if (benchmarks == "ycsbwklda") {
        YCSBWorkloadA(keys_file_type, keys_file_path, init_num_keys, batch_size,
                      print_batch_stats, time_limit, value_size);
    } else if (benchmarks == "realtime_ycsb") {
        ChangingWorkloadYCSB(total_num_keys, batch_size,time_limit, value_size, hot_time_frame,print_logs);
    } else if (benchmarks == "scan") {
        ScanHeavy(init_num_keys, batch_size);
    } else if (benchmarks == "ycsba_mt") {
        YCSBWorkloadA_mt(init_num_keys, batch_size, value_size);
    } else if (benchmarks == "realtime_ycsb_mt") {
        ChangingWorkloadYCSB_mt(total_num_keys, batch_size, time_limit, value_size, hot_time_frame, print_logs);
    } else if (benchmarks == "default"){
        default_benchmarks(keys_file_type, keys_file_path, init_num_keys, total_num_keys, batch_size,
                           insert_frac, lookup_distribution, print_batch_stats, time_limit);
    }
}

int default_benchmarks(std::string keys_file_type, std::string keys_file_path,
                       int init_num_keys, int total_num_keys, int batch_size, double insert_frac,
                       std::string lookup_distribution, bool print_batch_stats, double time_limit) {
    std::cout << "Running the default benchmarks" << std::endl;
    // Read keys from file
    double dummy_value = 8;
    RandomGenerator gen;
    auto keys = new KEY_TYPE[total_num_keys];
    if (keys_file_type == "binary") {
        load_binary_data(keys, total_num_keys, keys_file_path);
    } else if (keys_file_type == "text") {
        load_text_data(keys, total_num_keys, keys_file_path);
    } else {
        std::cerr << "--keys_file_type must be either 'binary' or 'text'"
                  << std::endl;
        return 1;
    }

    // Combine bulk loaded keys with randomly generated payloads
    auto values = new std::pair<KEY_TYPE, PAYLOAD_TYPE>[init_num_keys];
    std::mt19937_64 gen_payload(std::random_device{}());
    for (int i = 0; i < init_num_keys; i++) {
        values[i].first = keys[i];
        values[i].second = dummy_value;
    }

    std::cout << "start bulk loading" << std::endl;
    // Create ALEX and bulk load
    alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;
    std::sort(values, values + init_num_keys,
              [](auto const &a, auto const &b) { return a.first < b.first; });
    index.bulk_load(values, init_num_keys);

    std::cout << "Finished bulk loading" << std::endl;

    // Run workload
    int i = init_num_keys;
    long long cumulative_inserts = 0;
    long long cumulative_lookups = 0;
    int num_inserts_per_batch = static_cast<int>(batch_size * insert_frac);
    int num_lookups_per_batch = batch_size - num_inserts_per_batch;
    double cumulative_insert_time = 0;
    double cumulative_lookup_time = 0;

    auto workload_start_time = std::chrono::high_resolution_clock::now();
    int batch_no = 0;
//    PAYLOAD_TYPE sum = 0;
    std::cout << std::scientific;
    std::cout << std::setprecision(3);
    while (true) {
        batch_no++;

        // Do lookups
        KEY_TYPE *lookup_keys = nullptr;
        if (lookup_distribution == "uniform") {
            lookup_keys = get_search_keys(keys, i, num_lookups_per_batch);
        } else if (lookup_distribution == "zipf") {
            lookup_keys = get_search_keys_zipf(keys, i, num_lookups_per_batch);
        } else {
            std::cerr << "--lookup_distribution must be either 'uniform' or 'zipf'"
                      << std::endl;
            return 1;
        }
        auto lookups_start_time = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < num_lookups_per_batch; j++) {
            KEY_TYPE key = lookup_keys[j];
            index.get_payload(key);
//            if (payload) {
//                sum += *payload;
//            }
        }
        auto lookups_end_time = std::chrono::high_resolution_clock::now();
        double batch_lookup_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(lookups_end_time -
                                                                     lookups_start_time)
                        .count();
        cumulative_lookup_time += batch_lookup_time;
        cumulative_lookups += num_lookups_per_batch;
        delete[] lookup_keys;

        // Do inserts
        int num_actual_inserts =
                std::min(num_inserts_per_batch, total_num_keys - i);
        int num_keys_after_batch = i + num_actual_inserts;
        auto inserts_start_time = std::chrono::high_resolution_clock::now();
        for (; i < num_keys_after_batch; i++) {
            index.insert(keys[i], dummy_value);
        }
        auto inserts_end_time = std::chrono::high_resolution_clock::now();
        double batch_insert_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(inserts_end_time -
                                                                     inserts_start_time)
                        .count();
        cumulative_insert_time += batch_insert_time;
        cumulative_inserts += num_actual_inserts;

        if (print_batch_stats) {
            int num_batch_operations = num_lookups_per_batch + num_actual_inserts;
            double batch_time = batch_lookup_time + batch_insert_time;
            long long cumulative_operations = cumulative_lookups + cumulative_inserts;
            double cumulative_time = cumulative_lookup_time + cumulative_insert_time;
            std::cout << "Batch " << batch_no
                      << ", cumulative ops: " << cumulative_operations
                      << "\n\tbatch throughput:\t"
                      << num_lookups_per_batch / batch_lookup_time * 1e9
                      << " lookups/sec,\t"
                      << num_actual_inserts / batch_insert_time * 1e9
                      << " inserts/sec,\t" << num_batch_operations / batch_time * 1e9
                      << " ops/sec"
                      << "\n\tcumulative throughput:\t"
                      << cumulative_lookups / cumulative_lookup_time * 1e9
                      << " lookups/sec,\t"
                      << cumulative_inserts / cumulative_insert_time * 1e9
                      << " inserts/sec,\t"
                      << cumulative_operations / cumulative_time * 1e9 << " ops/sec"
                      << std::endl;
        }

        // Check for workload end conditions
        if (num_actual_inserts < num_inserts_per_batch) {
            // End if we have inserted all keys in a workload with inserts
            break;
        }
        double workload_elapsed_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(
                        std::chrono::high_resolution_clock::now() - workload_start_time)
                        .count();
        if (workload_elapsed_time > time_limit * 1e9 * 60) {
            break;
        }
    }

    long long cumulative_operations = cumulative_lookups + cumulative_inserts;
    double cumulative_time = cumulative_lookup_time + cumulative_insert_time;
    std::cout << "Cumulative stats: " << batch_no << " batches, "
              << cumulative_operations << " ops (" << cumulative_lookups
              << " lookups, " << cumulative_inserts << " inserts)"
              << "\n\tcumulative throughput:\t"
              << cumulative_lookups / cumulative_lookup_time * 1e9
              << " lookups/sec,\t"
              << cumulative_inserts / cumulative_insert_time * 1e9
              << " inserts/sec,\t"
              << cumulative_operations / cumulative_time * 1e9 << " ops/sec"
              << std::endl;

    delete[] keys;
    delete[] values;
    return 0;
}

int YCSBWorkloadA(std::string keys_file_type, std::string keys_file_path,
                  int init_num_keys, int batch_size, bool print_batch_stats, double time_limit, int value_size) {
    (void) (print_batch_stats);
    (void) (value_size);
    (void) (keys_file_type);
    (void) (keys_file_path);
    (void) (time_limit);
    std::cout << "Start to monitor YCSB A" << std::endl;

    RandomGenerator gen;
    //https://stackoverflow.com/questions/686353/random-float-number-generation
    std::random_device rd;
    std::mt19937 get_int_key(rd());
    std::uniform_int_distribution<> dist(0, std::numeric_limits<int>::max());

    // the total number of keys is kind of useless
    // as long as it is bigger than the init number
    std::set<KEY_TYPE> keys_set;
    int N = init_num_keys;
    int size = 0;
    while (size < N) {
        KEY_TYPE key = dist(get_int_key);
        if (keys_set.insert(key).second) {
            size++;
        }

        if (size % 500000 == 0) {
            std::cout << "Current size: " << size << std::endl;
        }
    }

    std::vector<KEY_TYPE> keys(keys_set.begin(), keys_set.end());
    std::cout << "Finished converting the set to vector" << std::endl;
    std::cout << keys.size() << std::endl;
    //double  dummy_value = 888;
    auto values = new std::pair<KEY_TYPE, PAYLOAD_TYPE>[init_num_keys];
    std::mt19937_64 gen_payload(std::random_device{}());
    for (int i = 0; i < init_num_keys; i++) {
        values[i].first = keys[i];
        //values[i].second = dummy_value;
    }

    std::cout << "Start to bulk loading..." << std::endl;
    // Create ALEX and bulk load
    alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;
    std::sort(values, values + init_num_keys,
              [](auto const &a, auto const &b) { return a.first < b.first; });
    std::cout << "Finished sorting" << std::endl;
    index.bulk_load(values, init_num_keys);
    std::cout << "Finished bulk loading and start to do the test" << std::endl;

    // set the key space
    KEY_TYPE min_key = values[0].first;
    KEY_TYPE max_key = values[init_num_keys - 1].first;

    std::cout << "The minimum key is " << min_key << std::endl;
    std::cout << "The potential maximum key is " << max_key << std::endl;

    LatencyDistributionRecorder dis_rec(1000, 0, 1000);

    // Run workload
    long long cumulative_inserts = 0;
    long long cumulative_lookups = 0;
    int num_inserts_per_batch = static_cast<int>(batch_size * 0.5);
    int num_lookups_per_batch = batch_size - num_inserts_per_batch;
    double cumulative_insert_time = 0;
    double cumulative_lookup_time = 0;
    int batch_no = 0;
    int total_ops = 10000000;

    while (true) {
        batch_no++;

//        auto start = std::chrono::high_resolution_clock::now();
//        auto end = std::chrono::high_resolution_clock::now();
        // -----------------Do lookups-----------------
        auto lookups_start_time = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < num_lookups_per_batch; j++) {
            index.get(dist(get_int_key));
        }
        auto lookups_end_time = std::chrono::high_resolution_clock::now();
        double batch_lookup_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(lookups_end_time - lookups_start_time).count();
        cumulative_lookup_time += batch_lookup_time;
        cumulative_lookups += num_lookups_per_batch;
        // -----------------Do lookups-----------------


        // -----------------Do inserts-----------------
        auto inserts_start_time = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < num_inserts_per_batch; j++) {
//            start = std::chrono::high_resolution_clock::now();
            //index.insert(dist(get_int_key), dummy_value);
//            end = std::chrono::high_resolution_clock::now();
//            double op_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
//            dis_rec.add(op_duration / 1000);
        }
        auto inserts_end_time = std::chrono::high_resolution_clock::now();
        double batch_insert_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(inserts_end_time - inserts_start_time).count();
        cumulative_insert_time += batch_insert_time;
        cumulative_inserts += num_inserts_per_batch;
        // -----------------Do inserts-----------------



//        auto current_time = std::chrono::high_resolution_clock::now();
//        auto time_since_last_log = std::chrono::duration_cast<std::chrono::nanoseconds>
//                (current_time - last_log_time).count();
//        if (time_since_last_log > log_time_frame * 1e9) {
//            sec++;
//            // print the time frame stats
//            int num_total_operations = log_num_insert + log_num_read;
//            double true_time_interval = log_read_time + log_insert_time;
//            if (true) {
//                std::cout << "Second " << sec
//                          << ", cumulative ops: " << num_total_operations
//                          << "\n\tone second throughput:\t"
//                          << log_num_read / log_read_time * 1e9
//                          << " lookups/sec,\t"
//                          << log_num_insert / log_insert_time * 1e9
//                          << " inserts/sec,\t" << num_total_operations / true_time_interval * 1e9
//                          << " ops/sec"
//                          << std::endl;
//            }
//
//            log_num_insert = 0;
//            log_num_read = 0;
//            log_read_time = 0;
//            log_insert_time = 0;
//            last_log_time = std::chrono::high_resolution_clock::now();
//        }
//
//        double workload_elapsed_time =
//                std::chrono::duration_cast<std::chrono::nanoseconds>(
//                        std::chrono::high_resolution_clock::now() - workload_start_time)
//                        .count();
//        if (workload_elapsed_time > time_limit * 1e9 * 60) {
//            break;
//        }

        if (cumulative_lookups + cumulative_inserts >= total_ops) {
            break;
        }
    }

//    dis_rec.print_stats(false);
//    std::cout << "Average time (microsec) for a split " << index.stats_.split_time / index.stats_.num_split << std::endl;
//    std::cout << "Average time (microsec) for a merge " << index.avg_merge_time() << std::endl;
    long long cumulative_operations = cumulative_lookups + cumulative_inserts;
    double cumulative_time = cumulative_lookup_time + cumulative_insert_time;
    std::cout << "Cumulative stats: " << batch_no << " batches, "
              << cumulative_operations << " ops (" << cumulative_lookups
              << " lookups, " << cumulative_inserts << " inserts)"
              << "\n\tcumulative throughput:\t"
              << cumulative_lookups / cumulative_lookup_time * 1e9
              << " lookups/sec,\t"
              << cumulative_inserts / cumulative_insert_time * 1e9
              << " inserts/sec,\t"
              << cumulative_operations / cumulative_time * 1e9 << " ops/sec"
              << std::endl;

    //delete[] keys;
    delete[] values;
    return 0;
}


int ChangingWorkloadYCSB(int total_num_keys, int batch_size,
                         double time_limit, int value_size, double hot_time_frame,
                         bool print_logs) {
    (void) (value_size);
    std::cout << "Start to monitor the dynamic workload." << std::endl;

    RandomGenerator gen;
    LatencyDistributionRecorder dis_rec(1000, 0, 1000);
    LatencyDistributionRecorder dis_rec_per_sec(1000, 0, 1000);

    //https://stackoverflow.com/questions/686353/random-float-number-generation
    std::random_device rd;
    std::mt19937 get_int_key(rd());
    std::uniform_int_distribution<> dist(0, std::numeric_limits<int>::max());

    // the total number of keys is kind of useless
    // as long as it is bigger than the init number
    std::set<KEY_TYPE> keys_set;
    int N = total_num_keys;
    int size = 0;
    while (size < N) {
        KEY_TYPE key = dist(get_int_key);
        if (keys_set.insert(key).second) {
            size++;
        }

        if (size % 10000 == 0) {
            std::cout << "Current size: " << size << std::endl;
        }
    }


    std::vector<KEY_TYPE> keys(keys_set.begin(), keys_set.end());
    std::cout << "Finished converting the set to vector" << std::endl;
    std::cout << keys.size() << std::endl;
    double dummy_value = 888;
    auto values = new std::pair<KEY_TYPE, PAYLOAD_TYPE>[total_num_keys];
    std::mt19937_64 gen_payload(std::random_device{}());
    for (int i = 0; i < total_num_keys; i++) {
        values[i].first = keys[i];
        values[i].second = dummy_value;
    }

    std::cout << "Start to bulk loading..." << std::endl;
    // Create ALEX and bulk load
    alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;
    std::sort(values, values + total_num_keys,
              [](auto const &a, auto const &b) { return a.first < b.first; });
    std::cout << "Finished sorting" << std::endl;
    index.bulk_load(values, total_num_keys);
    std::cout << "Finished bulk loading and start to do the test" << std::endl;

    // set the key space
    KEY_TYPE min_key = values[0].first;
    KEY_TYPE max_key = values[total_num_keys - 1].first;
    KEY_TYPE hot_range = static_cast<int>((max_key - min_key) * 0.01);

    std::cout << "The minimum key is " << min_key << std::endl;
    std::cout << "The potential maximum key is " << max_key << std::endl;

    std::mt19937 gen_prob(rd());
    std::uniform_real_distribution<> prob(0.0, 10.0);

    std::mt19937 gen_total(rd());
    std::uniform_int_distribution<> total(min_key, max_key);

    // Run workload
    int num_inserts_per_batch = static_cast<int>(batch_size * 0.5);
    int num_lookups_per_batch = batch_size - num_inserts_per_batch;

    auto workload_start_time = std::chrono::high_resolution_clock::now();
    int batch_no = 0;

    // for stats
    auto last_shift_change_point = std::chrono::high_resolution_clock::now();
    int shift_num = 1;
    auto last_log_time = std::chrono::high_resolution_clock::now();
    int log_time_frame = 1;
    double log_insert_time = 0;
    double log_read_time = 0;
    int log_num_insert = 0;
    int log_num_read = 0;
    int sec = 0;
    std::string content = "second_num,reads_per_sec,inserts_per_sec,ops_per_sec,median,99p_insert,99.9p_insert\n";
    std::string comma = ",";

    //https://stackoverflow.com/questions/686353/random-float-number-generation

    while (true) {
        batch_no++;

        KEY_TYPE lower_bound = static_cast<int>(min_key + shift_num * hot_range);
        KEY_TYPE upper_bound = static_cast<int>(min_key + (shift_num + 1) * hot_range);
        std::uniform_int_distribution<> dist(lower_bound, upper_bound);

        // -----------------Do lookups-----------------
        auto lookups_start_time = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < num_lookups_per_batch; j++) {
            KEY_TYPE lookup_key;
            if (prob(gen_prob) > 9.9) {
                lookup_key = total(gen_total);
            } else {
                lookup_key = dist(get_int_key);
            }
            index.get_payload(lookup_key);
        }
        auto lookups_end_time = std::chrono::high_resolution_clock::now();
        double batch_lookup_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(lookups_end_time - lookups_start_time).count();
        log_read_time += batch_lookup_time;
        log_num_read += num_lookups_per_batch;
        // -----------------Do lookups-----------------

        auto start = std::chrono::high_resolution_clock::now();
        auto end = std::chrono::high_resolution_clock::now();
        auto current_time = std::chrono::high_resolution_clock::now();
        auto hot_interval_last_time = std::chrono::duration_cast<std::chrono::nanoseconds>
                (current_time - last_shift_change_point).count();

        // change the hot area of the input workload
        if (hot_interval_last_time > hot_time_frame * 1e9) {
            last_shift_change_point = std::chrono::high_resolution_clock::now();
            shift_num++;
        }

        // -----------------Do writes-----------------
        auto inserts_start_time = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < num_inserts_per_batch; j++) {
            KEY_TYPE inserted_key;
            if (prob(gen_prob) > 9.9) {
                inserted_key = total(gen_total);
            } else {
                inserted_key = dist(get_int_key);
            }
            start = std::chrono::high_resolution_clock::now();
            index.insert(inserted_key, dummy_value);
            end = std::chrono::high_resolution_clock::now();
            double latency = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
            if (batch_no >= 100) {
                dis_rec.add(latency / 1000);
            }
            dis_rec_per_sec.add(latency / 1000);
        }
        auto inserts_end_time = std::chrono::high_resolution_clock::now();
        double batch_insert_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(inserts_end_time - inserts_start_time).count();
        log_insert_time += batch_insert_time;
        log_num_insert += num_inserts_per_batch;
        // -----------------Do writes-----------------

        current_time = std::chrono::high_resolution_clock::now();
        auto time_since_last_log = std::chrono::duration_cast<std::chrono::nanoseconds>
                (current_time - last_log_time).count();
        if (time_since_last_log > log_time_frame * 1e9) {
            sec++;
            // print the time frame stats
            int num_total_operations = log_num_insert + log_num_read;
            double true_time_interval = log_read_time + log_insert_time;
            if (print_logs) {
                std::cout << "Second " << sec
                          << ", cumulative ops: " << num_total_operations
                          << "\n\tone second throughput:\t"
                          << log_num_read / log_read_time * 1e9
                          << " lookups/sec,\t"
                          << log_num_insert / log_insert_time * 1e9
                          << " inserts/sec,\t" << num_total_operations / true_time_interval * 1e9
                          << " ops/sec"
                          << std::endl;

                dis_rec_per_sec.print_stats(false);
            }

            dis_rec_per_sec.reset();
            log_num_insert = 0;
            log_num_read = 0;
            log_read_time = 0;
            log_insert_time = 0;
            last_log_time = std::chrono::high_resolution_clock::now();
        }

        double workload_elapsed_time =
                std::chrono::duration_cast<std::chrono::nanoseconds>(
                        std::chrono::high_resolution_clock::now() - workload_start_time)
                        .count();
        if (workload_elapsed_time > time_limit * 1e9 * 60) {
            break;
        }
    }

//    dis_rec.print_stats(false);

    delete[] values;
    return 0;
}

const int thread_waiting = 3;
Barrier barrier(thread_waiting);
alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index_array[thread_waiting];
std::mutex locks[thread_waiting];
std::mutex global_mutex;

int ScanHeavy(int init_num_keys, int batch_size) {
    std::cout << "Start to monitor scan heavy" << std::endl;

    RandomGenerator gen;


    auto do_workloads = [](int thread_num, int init_num_keys, int batch_size, KEY_TYPE left, KEY_TYPE right) {
        // the total number of keys is kind of useless
        // as long as it is bigger than the init number
        //https://stackoverflow.com/questions/686353/random-float-number-generation
        std::random_device rd;
        std::mt19937 get_int_key(rd());
        std::uniform_int_distribution<> dist(left, right);

        std::set<KEY_TYPE> keys_set;
        int N = init_num_keys;
        int size = 0;
        keys_set.insert(left);
        keys_set.insert(right);
        while (size < N) {
            KEY_TYPE key = dist(get_int_key);
            if (keys_set.insert(key).second) {
                size++;
            }

            if (size % 500000 == 0) {
                std::cout << "Current size: " << size << std::endl;
            }
        }

        std::vector<KEY_TYPE> keys(keys_set.begin(), keys_set.end());
        std::cout << "Finished converting the set to vector" << std::endl;
        double dummy_value = 888;
        auto values = new std::pair<KEY_TYPE, PAYLOAD_TYPE>[init_num_keys];
        std::mt19937_64 gen_payload(std::random_device{}());
        for (int i = 0; i < init_num_keys; i++) {
            values[i].first = keys[i];
            values[i].second = dummy_value;
        }

        std::cout << "Start to bulk loading..." << std::endl;
        // Create ALEX and bulk load
        alex::Alex<KEY_TYPE, PAYLOAD_TYPE> &index = index_array[thread_num];
        std::sort(values, values + init_num_keys,
                  [](auto const &a, auto const &b) { return a.first < b.first; });
        std::cout << "Finished sorting" << std::endl;
        index.bulk_load(values, init_num_keys);
        std::cout << "Finished bulk loading and start to do the test" << std::endl;

        // set the key space
        KEY_TYPE min_key = values[0].first;
        KEY_TYPE max_key = values[init_num_keys - 1].first;
        KEY_TYPE shard_range = static_cast<int> (max_key - min_key);
        KEY_TYPE lower_bound = left;
        KEY_TYPE upper_bound = right;
        std::uniform_int_distribution<> correct_dist(lower_bound, upper_bound);

//        std::cout << "The minimum key is " << min_key << std::endl;
//        std::cout << "The potential maximum key is " << max_key << std::endl;

        // Run workload
        long long cumulative_inserts = 0;
        long long cumulative_lookups = 0;
        int num_inserts_per_batch = static_cast<int>(batch_size * 0.05);
        int num_lookups_per_batch = batch_size - num_inserts_per_batch;
        double cumulative_insert_time = 0;
        double cumulative_lookup_time = 0;
        int batch_no = 0;
        int total_ops = 10000000;
        std::mutex &lock = locks[thread_num];
        barrier.wait();
        std::string info = "This thread " + std::to_string(thread_num) + " has started!!!";
        std::cout << info << std::endl;

        while (true) {
            batch_no++;

            // -----------------Do lookups-----------------
            auto lookups_start_time = std::chrono::high_resolution_clock::now();
            for (int j = 0; j < num_lookups_per_batch;) {
                KEY_TYPE key = correct_dist(get_int_key);
                KEY_TYPE requested_right_bound = key + 500;
                // try to determine the space of a scan
                if (requested_right_bound > upper_bound) {
                    global_mutex.lock();
                    int i = thread_num;
                    while (true) {
                        locks[i].lock();
                        KEY_TYPE lower_key = (key < i * shard_range) ? i * shard_range : key;
                        KEY_TYPE upper_key = (requested_right_bound < (i + 1) * shard_range) ?
                                             requested_right_bound : (i + 1) * shard_range;
                        index_array[i].scan_unsorted(lower_key, upper_key);
                        locks[i].unlock();
                        if ((i + 1) * shard_range > requested_right_bound || i + 1 >= thread_num) {
                            break;
                        }
                        i++;
                    }
                    global_mutex.unlock();
                } else {
                    lock.lock();
                    index.scan_unsorted(key, requested_right_bound);
                    lock.unlock();
                }
                j++;
            }
            auto lookups_end_time = std::chrono::high_resolution_clock::now();
            double batch_lookup_time =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(lookups_end_time - lookups_start_time).count();
            cumulative_lookup_time += batch_lookup_time;
            cumulative_lookups += num_lookups_per_batch;
            // -----------------Do lookups-----------------


            // -----------------Do inserts-----------------
            auto inserts_start_time = std::chrono::high_resolution_clock::now();
            for (int j = 0; j < num_inserts_per_batch;) {
                KEY_TYPE key = dist(get_int_key);
                lock.lock();
                index.insert(key, dummy_value);
                lock.unlock();
                j++;
            }
            auto inserts_end_time = std::chrono::high_resolution_clock::now();
            double batch_insert_time =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(inserts_end_time - inserts_start_time).count();
            cumulative_insert_time += batch_insert_time;
            cumulative_inserts += num_inserts_per_batch;
            // -----------------Do inserts-----------------

            if (cumulative_lookups + cumulative_inserts >= total_ops) {
                break;
            }
        }

        long long cumulative_operations = cumulative_lookups + cumulative_inserts;
        double cumulative_time = cumulative_lookup_time + cumulative_insert_time;
        std::cout << "Cumulative stats: " << batch_no << " batches, "
                  << cumulative_operations << " ops (" << cumulative_lookups
                  << " lookups, " << cumulative_inserts << " inserts)"
                  << "\n\tcumulative throughput:\t"
                  << cumulative_lookups / cumulative_lookup_time * 1e9
                  << " lookups/sec,\t"
                  << cumulative_inserts / cumulative_insert_time * 1e9
                  << " inserts/sec,\t"
                  << cumulative_operations / cumulative_time * 1e9 << " ops/sec"
                  << std::endl;
        delete[] values;
    };

    // initialize the indexes
    for (int i = 0; i < thread_waiting; i++) {
        alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;
        index_array[i] = index;
    }
    std::thread threads[thread_waiting];
    KEY_TYPE shard_range = 1000000;
    for (int i = 0; i < thread_waiting; i++) {
        threads[i] = std::thread(do_workloads, i, init_num_keys, batch_size, i * shard_range, (i + 1) * shard_range);
    }

    for (auto &t: threads) {
        t.join();
    }

    return 0;
}

int YCSBWorkloadA_mt(int init_num_keys, int batch_size, int value_size) {
    (void) (value_size);
    std::cout << "Start to monitor YCSB A for multithreading" << std::endl;

    RandomGenerator gen;

    auto do_workloads = [](int thread_num, int init_num_keys, int batch_size, KEY_TYPE left, KEY_TYPE right) {
        // the total number of keys is kind of useless
        // as long as it is bigger than the init number
        //https://stackoverflow.com/questions/686353/random-float-number-generation
        std::random_device rd;
        std::mt19937 get_int_key(rd());
        std::uniform_int_distribution<> dist(left, right);

        std::set<KEY_TYPE> keys_set;
        int N = init_num_keys;
        int size = 0;
        while (size < N) {
            KEY_TYPE key = dist(get_int_key);
            if (keys_set.insert(key).second) {
                size++;
            }

//            if (size % 50000 == 0) {
//                std::cout << "Current size: " << size << std::endl;
//            }
        }

        std::vector<KEY_TYPE> keys(keys_set.begin(), keys_set.end());
        std::cout << "Finished converting the set to vector" << std::endl;
        std::cout << keys.size() << std::endl;
        double dummy_value = 888;
        auto values = new std::pair<KEY_TYPE, PAYLOAD_TYPE>[init_num_keys];
        std::mt19937_64 gen_payload(std::random_device{}());
        for (int i = 0; i < init_num_keys; i++) {
            values[i].first = keys[i];
            values[i].second = dummy_value;
        }

        std::cout << "Start to bulk loading..." << std::endl;
        // Create ALEX and bulk load
        alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;
        std::sort(values, values + init_num_keys,
                  [](auto const &a, auto const &b) { return a.first < b.first; });
        std::cout << "Finished sorting" << std::endl;
        index.bulk_load(values, init_num_keys);
        std::cout << "Finished bulk loading and start to do the test" << std::endl;

//        // set the key space
//        KEY_TYPE min_key = values[0].first;
//        KEY_TYPE max_key = values[init_num_keys - 1].first;

//        std::cout << "The minimum key is " << min_key << std::endl;
//        std::cout << "The potential maximum key is " << max_key << std::endl;

        // Run workload
        long long cumulative_inserts = 0;
        long long cumulative_lookups = 0;
        int num_inserts_per_batch = static_cast<int>(batch_size * 0.5);
        int num_lookups_per_batch = batch_size - num_inserts_per_batch;
        double cumulative_insert_time = 0;
        double cumulative_lookup_time = 0;
        int batch_no = 0;
        int total_ops = 10000000;

        barrier.wait();
        std::string info = "This thread " + std::to_string(thread_num) + " has started!!!";
        std::cout << info << std::endl;

        while (true) {
            batch_no++;

            // -----------------Do lookups-----------------
            auto lookups_start_time = std::chrono::high_resolution_clock::now();
            for (int j = 0; j < num_lookups_per_batch;) {
                index.get(dist(get_int_key));
                j++;
            }
            auto lookups_end_time = std::chrono::high_resolution_clock::now();
            double batch_lookup_time =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(lookups_end_time - lookups_start_time).count();
            cumulative_lookup_time += batch_lookup_time;
            cumulative_lookups += num_lookups_per_batch;
            // -----------------Do lookups-----------------


            // -----------------Do writes-----------------
            auto inserts_start_time = std::chrono::high_resolution_clock::now();
            for (int j = 0; j < num_inserts_per_batch;) {
                KEY_TYPE key = dist(get_int_key);
                index.insert(key, dummy_value);
                j++;
            }
            auto inserts_end_time = std::chrono::high_resolution_clock::now();
            double batch_insert_time =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(inserts_end_time - inserts_start_time).count();
            cumulative_insert_time += batch_insert_time;
            cumulative_inserts += num_inserts_per_batch;
            // -----------------Do inserts-----------------

            if (cumulative_lookups + cumulative_inserts >= total_ops) {
                break;
            }
        }

        long long cumulative_operations = cumulative_lookups + cumulative_inserts;
        double cumulative_time = cumulative_lookup_time + cumulative_insert_time;
        std::cout << "Cumulative stats: " << batch_no << " batches, "
                  << cumulative_operations << " ops (" << cumulative_lookups
                  << " lookups, " << cumulative_inserts << " inserts)"
                  << "\n\tcumulative throughput:\t"
                  << cumulative_lookups / cumulative_lookup_time * 1e9
                  << " lookups/sec,\t"
                  << cumulative_inserts / cumulative_insert_time * 1e9
                  << " inserts/sec,\t"
                  << cumulative_operations / cumulative_time * 1e9 << " ops/sec"
                  << std::endl;
        delete[] values;
    };

    std::thread threads[thread_waiting];
    KEY_TYPE shard_range = 1000000;
    for (int i = 0; i < thread_waiting; i++) {
        threads[i] = std::thread(do_workloads, i, init_num_keys, batch_size, i * shard_range, (i + 1) * shard_range);
    }

    for (auto &t: threads) {
        t.join();
    }

    return 0;
}

int ChangingWorkloadYCSB_mt(int total_num_keys, int batch_size,
                            double time_limit, int value_size, double hot_time_frame,
                            bool print_logs) {
    (void) (value_size);
    std::cout << "Start to monitor dynamic workloads in multithreading" << std::endl;

    RandomGenerator gen;

    auto do_workloads = [](int thread_num, int init_num_keys, int batch_size,
                           bool print_logs, int hot_time_frame, int time_limit) {
        RandomGenerator gen;

        // the total number of keys is kind of useless
        // as long as it is bigger than the init number
        std::random_device rd;
        std::mt19937 get_int_key(rd());
        KEY_TYPE range = static_cast<int>((std::numeric_limits<int>::max() - 100) / thread_waiting);
        KEY_TYPE start = thread_num * range;
        KEY_TYPE end = (thread_num + 1) * range;
        std::uniform_int_distribution<> dist(start, end);

        // the total number of keys is kind of useless
        // as long as it is bigger than the init number
        std::set<KEY_TYPE> keys_set;
        int N = init_num_keys;
        int size = 0;
        while (size < N) {
            KEY_TYPE key = dist(get_int_key);
            if (keys_set.insert(key).second) {
                size++;
            }

//            if (size % 10000 == 0) {
//                std::cout << "Current size: " << size << std::endl;
//            }
        }

        std::vector<KEY_TYPE> keys(keys_set.begin(), keys_set.end());
        std::cout << "Finished converting the set to vector" << std::endl;
        std::cout << keys.size() << std::endl;
        double dummy_value = 888;
        auto values = new std::pair<KEY_TYPE, PAYLOAD_TYPE>[init_num_keys];
        std::mt19937_64 gen_payload(std::random_device{}());
        for (int i = 0; i < init_num_keys; i++) {
            values[i].first = keys[i];
            values[i].second = dummy_value;
        }

        std::cout << "Start to bulk loading..." << std::endl;
        // Create ALEX and bulk load
        alex::Alex<KEY_TYPE, PAYLOAD_TYPE> index;
        std::sort(values, values + init_num_keys,
                  [](auto const &a, auto const &b) { return a.first < b.first; });
        std::cout << "Finished sorting" << std::endl;
        // std::cout << "Start bulk loading and start to do the test" << std::endl;
        index.bulk_load(values, init_num_keys);
        std::cout << "Finished bulk loading and start to do the test" << std::endl;

        // set the key space
        KEY_TYPE min_key = values[0].first;
        KEY_TYPE max_key = values[init_num_keys - 1].first;
        KEY_TYPE hot_range = static_cast<int>((max_key - min_key) * 0.01);

//        std::cout << "The minimum key is " << min_key << std::endl;
//        std::cout << "The potential maximum key is " << max_key << std::endl;


        // Run workload
        int num_inserts_per_batch = static_cast<int>(batch_size * 0.5);
        int num_lookups_per_batch = batch_size - num_inserts_per_batch;

        auto workload_start_time = std::chrono::high_resolution_clock::now();
        int batch_no = 0;

        // for stats
        auto last_shift_change_point = std::chrono::high_resolution_clock::now();
        int shift_num = 1;
        auto last_log_time = std::chrono::high_resolution_clock::now();
        int log_time_frame = 1;
        double log_insert_time = 0;
        double log_read_time = 0;
        int log_num_insert = 0;
        int log_num_read = 0;
        int sec = 0;
        std::string content = "second_num,reads_per_sec,inserts_per_sec,ops_per_sec,median,99p_insert,99.9p_insert\n";
        std::string comma = ",";

        //https://stackoverflow.com/questions/686353/random-float-number-generation
        std::mt19937 gen_int_key(rd());

        std::mt19937 gen_total(rd());
        std::uniform_int_distribution<> total(start, end);
        std::mt19937 gen_prob(rd());
        std::uniform_real_distribution<> prob(0.0, 10.0);


        while (true) {
            batch_no++;

            KEY_TYPE lower_bound = static_cast<KEY_TYPE>(min_key + shift_num * hot_range);
            KEY_TYPE upper_bound = static_cast<KEY_TYPE>(min_key + (shift_num + 1) * hot_range);
            std::uniform_int_distribution<> dist(lower_bound, upper_bound);

            // -----------------Do lookups-----------------
            auto lookups_start_time = std::chrono::high_resolution_clock::now();
            for (int j = 0; j < num_lookups_per_batch; j++) {
                KEY_TYPE lookup_key;
                if (prob(gen_prob) > 9.9) {
                    lookup_key = total(gen_total);
                } else {
                    lookup_key = dist(get_int_key);
                }
                index.get(lookup_key);
            }
            auto lookups_end_time = std::chrono::high_resolution_clock::now();
            double batch_lookup_time =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(lookups_end_time - lookups_start_time).count();
            log_read_time += batch_lookup_time;
            log_num_read += num_lookups_per_batch;
            // -----------------Do lookups-----------------

            auto current_time = std::chrono::high_resolution_clock::now();
            auto hot_interval_last_time = std::chrono::duration_cast<std::chrono::nanoseconds>
                    (current_time - last_shift_change_point).count();

            // change the hot area of the input workload
            if (hot_interval_last_time > hot_time_frame * 1e9) {
                last_shift_change_point = std::chrono::high_resolution_clock::now();
                shift_num++;
            }

            // -----------------Do writes-----------------
            auto inserts_start_time = std::chrono::high_resolution_clock::now();
            for (int j = 0; j < num_inserts_per_batch; j++) {
                KEY_TYPE inserted_key;
                if (prob(gen_prob) > 9.9) {
                    inserted_key = total(gen_total);
                } else {
                    inserted_key = dist(get_int_key);
                }
                index.insert(inserted_key, dummy_value);
            }
            auto inserts_end_time = std::chrono::high_resolution_clock::now();
            double batch_insert_time =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(inserts_end_time - inserts_start_time).count();
            log_insert_time += batch_insert_time;
            log_num_insert += num_inserts_per_batch;
            // -----------------Do writes-----------------

            current_time = std::chrono::high_resolution_clock::now();
            auto time_since_last_log = std::chrono::duration_cast<std::chrono::nanoseconds>
                    (current_time - last_log_time).count();
            if (time_since_last_log > log_time_frame * 1e9) {
                sec++;
                // print the time frame stats for one thread
                int num_total_operations = log_num_insert + log_num_read;
                double true_time_interval = log_read_time + log_insert_time;
                if (thread_num == 0 && print_logs) {
                    std::cout << "Second " << sec
                              << ", cumulative ops: " << num_total_operations
                              << "\n\tone second throughput:\t"
                              << log_num_read / log_read_time * 1e9
                              << " lookups/sec,\t"
                              << log_num_insert / log_insert_time * 1e9
                              << " inserts/sec,\t" << num_total_operations / true_time_interval * 1e9
                              << " ops/sec"
                              << std::endl;
                }
                log_num_insert = 0;
                log_num_read = 0;
                log_read_time = 0;
                log_insert_time = 0;
                last_log_time = std::chrono::high_resolution_clock::now();
            }

            double workload_elapsed_time =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(
                            std::chrono::high_resolution_clock::now() - workload_start_time)
                            .count();
            if (workload_elapsed_time > time_limit * 1e9 * 60) {
                break;
            }
        }
    };

    std::thread threads[thread_waiting];
    for (int i = 0; i < thread_waiting; i++) {
        threads[i] = std::thread(do_workloads, i, total_num_keys, batch_size, print_logs, hot_time_frame, time_limit);
    }

    for (auto &t: threads) {
        t.join();
    }

    return 0;
}
